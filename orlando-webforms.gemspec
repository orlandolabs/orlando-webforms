Gem::Specification.new do |s|
  s.name = 'orlando-webforms'
  s.version = '0.1.0'
  s.date = '2018-03-05'
  s.summary = 'Web forms for Rails'
  s.description = 'Web forms for variable multiple associated resources'
  s.authors = %w(Patrick Jane)
  s.email = 'team@orlando-labs.com'
  s.files = %w(lib/orlando-webforms.rb)
  s.license = 'Nonstandard'
  s.homepage = 'http://orlando-labs.com'

  s.add_dependency 'nokogiri'
end
