require 'nokogiri'
require 'active_support/all'

module Orlando
  class FormBuilder < ActionView::Helpers::FormBuilder
    class TableInput
      attr_reader :elements
      
      MATCH = {
        text: :text_field,
        number: :number_field,
        select: :select,
        collection_select: :collection_select,
      }

      def initialize
        @elements = []
      end

      def push(*args, **opts)
        hdr, *rest = args
        @elements << [MATCH[__callee__], hdr, rest, opts]
      end
      
      MATCH.keys.each { |m| alias_method m, :push }
    end

    REPLACEABLE = '%_replacable_template_%'

    private
    def sanitized_object_name
      @sanitized_object_name ||= @object_name.to_s.gsub(/\]\[|[^-a-zA-Z0-9:.]/, "_").sub(/_$/, "")
    end

    def i18n_scope
      parts = @object_name.to_s.gsub('[', '.').gsub(']', '').split '.'
      parts.map(&:pluralize).join('.') + '.form_fields'
    end

    def element_name(attr)
      "#{@object_name}[#{attr}]"
    end
    
    def element_id(attr)
      "#{sanitized_object_name}_#{attr}"
    end

    def associated_class(attr)
      Object.const_get "#{@object.class.name.deconstantize}::#{@object.class.reflect_on_association(attr).class_name}"
    end
    
    public
    def group(&block)
      if @already_groupped
        @template.capture(&block)
      else
        @already_groupped = true
        ret = @template.content_tag :div, @template.capture(&block), class: 'form-group'
        @already_groupped = false
        ret
      end
    end

    def method_missing(mth, *args, **opts, &block)
      prefix = mth.to_s.scan(/^lb_rq_|^rq_lb_|^lb_|^rq_/).first || ''
      add = prefix =~ /rq_/ ? {required: :required} : {}
      base_mth = prefix.present? ? mth.to_s.gsub(prefix, '') : mth.to_s
      if prefix =~ /lb_/
        if base_mth == 'check_box'
          group { "#{ @template.content_tag :div, "#{send(base_mth, *args, **opts.deep_merge({class: 'form-check-input'}.merge(add)), &block)}#{_label args.first, class: 'form-check-label'}".html_safe, class: 'form-check' }".html_safe }
        else
          group { "#{_label args.first}#{send(base_mth, *args, **opts.deep_merge(add), &block)}".html_safe }
        end
      else
        if respond_to? base_mth
          group { send(base_mth, *args, **opts.deep_merge(add), &block)}
        else
          super
        end
      end
    end

    %i(label text_field email_field password_field select text_area wysiwyg_text_area submit secondary_button table_input number_field check_box collection_select).each do |m|
      define_method m do |*args, **opts, &block|
        add = {}
        (opts[:class] ? opts[:class].concat(' is-invalid') : opts[:class] = 'is-invalid') if @object.has_attribute? args.first and @object.errors.include? args.first
        group do
          "#{send("_#{__callee__}", *args, **opts, &block)}#{@template.content_tag :div, ((args.first ? @object.errors[args.first] : nil) || @template.t(args.first, scope: "#{i18n_scope}.validations")), class: 'invalid-feedback'}".html_safe
        end
      end
    end

    def _label(attr, *args, **opts)
      sm = method(__callee__.to_s[1..-1]).super_method
      sm.call attr, (attr.is_a?(Symbol) ? @template.t(attr, scope: i18n_scope) : attr.to_s.humanize), *args, **{class: 'control-label'}.merge(opts)
    end

    def _text_field(*args, **opts)
      sm = method(__callee__.to_s[1..-1]).super_method
      sm.call *args, **{class: 'form-control underlined'}.deep_merge(opts)
    end
    
    def _email_field(*args, **opts)
      sm = method(__callee__.to_s[1..-1]).super_method
      sm.call *args, **{class: 'form-control underlined'}.deep_merge(opts)
    end
    
    def _check_box(*args, **opts)
      sm = method(__callee__.to_s[1..-1]).super_method
      sm.call *args, **{class: 'checkbox'}.deep_merge(opts)
    end
    
    def _password_field(*args, **opts)
      sm = method(__callee__.to_s[1..-1]).super_method
      sm.call *args, **{class: 'form-control underlined'}.deep_merge(opts)
    end
    
    def _number_field(*args, **opts)
      sm = method(__callee__.to_s[1..-1]).super_method
      sm.call *args, **{step: :any, class: 'form-control underlined'}.deep_merge(opts)
    end

    def _select(attr, choices, options = {}, **html)
      sm = method(__callee__.to_s[1..-1]).super_method
      sm.call attr, choices, options, **{class: 'form-control'}.deep_merge(html)
    end
    
    def _collection_select(attr, c, k, v, options = {}, **html)
      sm = method(__callee__.to_s[1..-1]).super_method
      sm.call attr, c, k, v, options, **{class: 'form-control'}.deep_merge(html)
    end

    def _text_area(*args, **opts)
      sm = method(__callee__.to_s[1..-1]).super_method
      sm.call *args, **{class: 'form-control'}.deep_merge(opts)
    end

    def _wysiwyg_text_area(*args, **opts)
      sm = method(:text_area).super_method
      "#{sm.call *args, **{class: 'tinymce'}.deep_merge(opts)}#{@template.tinymce(opts.delete :config)}".html_safe
    end

    def _html_erb_editor(attr, *args, **opts)
      out = """<div id=\"#{element_id attr}\" style=\"width: 100%; height: 600px;\"></div>
      <script>
        var #{element_id attr}_editor = ace.edit(\"#{element_id attr}\");
        var #{element_id attr}_editor_mode = new ace.require(\"ace/mode/html_ruby\").Mode;
        #{element_id attr}_editor.session.setMode(new #{element_id attr}_editor_mode());
      </script>".html_safe
    end

    def _submit(*args, **opts)
      sm = method(__callee__.to_s[1..-1]).super_method
      sm.call *args, **{class: 'btn btn-primary'}.deep_merge(opts)
    end

    def _secondary_button(*args, **opts)
      caption = args.first.is_a?(Symbol) ? @template.t(args.first, scope: i18n_scope) : args.first
      if fa = opts.delete(:fa_icon)
        caption = @template.fa_icon(fa, text: caption)
      end
      @template.link_to caption, 'javascript:void(0);', **{class: 'btn btn-secondary btn-sm'}.merge(opts)
    end

    def _table_input(attr, **opts, &block)
      opts[:on] = {} unless opts[:on]
      input_style = "margin: 0 !important; border: 0 !important; width: 100% !important; border-radius: 0 !important; line-height: 1 !important; display: none;"
      td_style = "margin: 0 !important; padding: 0 !important;"
      
      ti = TableInput.new
      block.call ti
      
      itypes = ti.elements.map &:first
      subattrs = ti.elements.map &:second
      args = ti.elements.map { |e| e[2] }
      _opts = ti.elements.map &:last
      
      rclass = associated_class attr
      trs, empty_row = nil, nil
      ths = '<th></th>'.concat subattrs.map { |th| "<th>#{@template.t(th, scope: "#{i18n_scope}.#{attr.to_s.pluralize}")}</th>" }.join
      *trs, empty_row = [*@object.send(attr).to_a, rclass.new].map.with_index do |v, idx|
        fields_for attr, v, include_id: false do |af|
          """<tr data-uid=\"#{idx}\">
            <td class=\"tiny\" style=\"#{td_style}\" data-service=\"true\">
              #{af.hidden_field rclass.primary_key, id: :id}
              #{af.hidden_field :_destroy, id: :destroyer, value: 0}
              <div class=\"dropdown text-center\">
                <a id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" href=\"#\">
                  <i class=\"fas fa-bars\"></i>
                </a>
                <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                  <a class=\"dropdown-item\" id=\"row-inserter-before\" href=\"#\" onclick=\"insert#{element_id(attr).camelize}Row({before: this}); #{opts[:on][:insert_before] || opts[:on][:appear]}; return false;\"><i class=\"far fa-sign-out-alt fa-rotate-270\"></i> #{@template.t(:insert_row_before, scope: 'menu_items.table_input')}</a>
                  <a class=\"dropdown-item\" id=\"row-inserter-after\" href=\"#\" onclick=\"insert#{element_id(attr).camelize}Row({after: this}); #{opts[:on][:insert_after] || opts[:on][:appear]}; return false;\"><i class=\"far fa-sign-out-alt fa-rotate-90\"></i> #{@template.t(:insert_row_after, scope: 'menu_items.table_input')}</a>
                  <a class=\"dropdown-item\" id=\"row-marker\" href=\"#\" onclick=\"markRowForDelete(this); #{opts[:on][:mark]}; return false;\"><span class=\"text-danger\"><i class=\"far fa-marker\"></i> #{@template.t(:mark_for_delete, scope: 'menu_items.table_input')}</span></a>
                  <a class=\"dropdown-item\" id=\"row-unmarker\" href=\"#\" onclick=\"unmarkRowForDelete(this); #{opts[:on][:unmark]}; return false;\" style=\"display: none;\"><span class=\"text-info\"><i class=\"far fa-marker\"></i> #{@template.t(:unmark_for_delete, scope: 'menu_items.table_input')}</span></a>
                  <a class=\"dropdown-item\" id=\"row-remover\" href=\"#\" onclick=\"deleteRow(this); #{opts[:on][:delete]}; return false;\"><span class=\"text-danger\"><i class=\"far fa-trash-alt\"></i> #{@template.t(:delete_row, scope: 'menu_items.table_input')}</span></a>
                </div>
              </div>
            </td>
            #{subattrs.map.with_index do |a, i|
              "<td data-num=\"#{i}\"#{" data-readonly" if _opts[i][:readonly] == true} data-attr=\"#{a}\" onclick=\"highlightCell($(this));\" ondblclick=\"unlockCell($(this));\" style=\"#{td_style}\"><span data-attr=\"#{a}\" class=\"val\">#{v[a]}</span>#{af.send("_#{itypes[i]}", a, *args[i], **(_opts[i].merge(class: '', style: input_style, data: {attr: a}, readonly: _opts[i][:readonly] == true)))}</td>"
            end.join}
          </tr>".html_safe
        end
      end
      dom = Nokogiri::HTML.fragment empty_row
      dom.xpath(".//input|select").map do |el|
        el.attributes['name'].value = el.attributes['name'].value.gsub /([a-z_\d\[\]]+\[#{attr}_attributes\])\[#{trs.count}\]/, "\\1[#{REPLACEABLE}]"
        el.attributes['id'].value = el.attributes['id'].value.gsub /(#{element_id(attr)}_attributes)_#{trs.count}_/, "\\1_#{REPLACEABLE}_" 
      end
      dom.xpath("tr")[0].attributes["data-uid"].value = REPLACEABLE
      empty_row_tpl = dom.to_html
      
      tpl = """<script>
        function fixColumnWidth($tbl){
          $.each($tbl.find('th'), function(th_idx,th){
            $(th).width($(th).width());
          });
        }
        function highlightCell($c) {
          $('td.highlighted').removeClass('highlighted');
          $c.addClass('highlighted');
        }
        function unlockCell($c) {
          if (typeof($c.data('readonly')) != \"undefined\") {
            return
          }
          $c.removeAttr('ondblclick');
          fixColumnWidth($c.closest('table'));
          highlightCell($c);
          //lock prev. open cells
          $.each( 
            $c.closest('table').find('input:visible,select:visible'), 
            function(idx, obj) {
              lockCell($(obj).closest('td'));
            }
          );

          //do unlock
          $c.addClass('editable');
          $v = $c.find('span.val');
          $v.hide();
          $i = $c.find('input');
          if ($i.length > 0) {
            txt = $i.val()
            $i.show().focus().val(txt); //move cursor to end
          } else { //select
            $c.find('select').show().focus();
          }
        }
        function lockCell($td){
          $td.removeClass('editable')
          $td.attr('ondblclick', 'unlockCell($(this))');
          $v = $td.find('span.val');
          $i = $td.find('input')

          if ($i.length > 0) {
            val = $i.val()
            if ($v.text() != val) {
              $v.text(val)
              $i.change()
            }
            $v.show()
            $i.hide()
          } else {
            $i = $td.find('select')
            val = $i.find('option:selected').val()
            if (val != $v.text()) {
              $v.text(val)
              $i.change()
            }
            $v.show()
            $i.hide();
          }
        }
        function has#{element_id(attr).camelize}Data(row) {
          any = false;
          row.find('input[data-attr]').each(function(idx) {
            if ($(this).val() != '') {
              any = true;
              return false;
            }
          });
          return any;
        }
        function set#{element_id(attr).camelize}HandlersFor(sel) {
          sel
            .on('input', inputHandlerFor#{element_id(attr).camelize})
            .on('keydown', keydownHandlerFor#{element_id(attr).camelize})
            .on('paste drop', pasteHandlerFor#{element_id(attr).camelize})
            .on('focusout', focusoutHandler)
        }
        function focusoutHandler(event) {
          lockCell($(event.currentTarget).closest('td'));
        }
        function keydownHandlerFor#{element_id(attr).camelize}(event) {
          var next = [];

          if (event.keyCode == 13) {
            event.preventDefault();
            next = $(event.currentTarget).closest('tr:visible').next('tr:visible').find('td[data-num=\"' + $(event.currentTarget).closest('td').data('num') + '\"]');
          } else if (event.keyCode == 9) {
            event.preventDefault();
            next = $(event.currentTarget).closest('td:visible').next('td:visible');
            if (next.length == 0) {
              next = $(event.currentTarget).closest('tr').next('tr:visible').find('td[data-num=\"0\"]');
            }
          } else if (event.keyCode == 27) {
            lockCell($(event.currentTarget).closest('td'));
          }
          if (next.length != 0) {
            unlockCell(next);
          }
        }
        function deleteRow(_this) {
          tr = $(_this).closest('tr');
          if (has#{element_id(attr).camelize}Data(tr) || (tr.next('tr:visible').length > 0)) {
            if (tr.find('input#id').val() != '') {
              tr.hide();
              tr.find('input#destroyer').val('1');
            } else {
              tr.remove();
            }
          }
          return false;
        }
        function markRowForDelete(_this) {
          tr = $(_this).closest('tr');
          if (has#{element_id(attr).camelize}Data(tr) || (tr.next('tr:visible').length > 0)) {
            tr.find('input#destroyer').val('1');
            tr.find('td').addClass('marked-for-delete');
            tr.find('span').addClass('stroke');
            tr.find('a#row-unmarker').show();
            tr.find('a#row-marker').hide();
          }
          return false;
        }
        function unmarkRowForDelete(_this) {
          tr = $(_this).closest('tr');
          tr.find('input#destroyer').val('0');
          tr.find('td').removeClass('marked-for-delete');
          tr.find('span').removeClass('stroke');
          tr.find('a#row-unmarker').hide();
          tr.find('a#row-marker').show();
          return false;
        }
        function select#{element_id(attr).camelize}Row(attr, val) {
          ret = null;
          $.each($(\"table##{element_id attr}\").find(\"input[data-attr='\" + attr + \"']\"), function(id, inp) {
            if ($(inp).val() === val.toString()) {
                ret = $(inp).closest('tr');
                return false;
            }
          });
          return ret;
        }
        function select#{element_id(attr).camelize}Col(id_attr, val, attr) {
          row = select#{element_id(attr).camelize}Row(id_attr, val);
          return row.find(\"td[data-attr='\" + attr + \"']\");
        }
        function update#{element_id(attr).camelize}Col(row, attr, val) {
          td = row.find(\"td[data-attr='\" + attr + \"']\")
          td.find(\"input\").val(val)
          td.find(\"span.val\").text(val);
        }
        #{"""function insert#{element_id(attr).camelize}Row(data) {
          last = null;
          t = $(data.after || data.before).closest('table')
          next_id = parseInt(t.attr('data-max-uid'), 10) + 1;
          t.attr('data-max-uid', next_id);
          empty_row = '#{@template.escape_javascript empty_row_tpl}'.replace(/#{REPLACEABLE}/g, next_id);
          if (typeof(data.after) != 'undefined') {
            last = data.after;
            last.after(empty_row);
            set#{element_id(attr).camelize}HandlersFor(last.next('tr:visible').find('input'));
          } else {
            last = data.before;
            last.before(empty_row);
            set#{element_id(attr).camelize}HandlersFor(last.prev('tr:visible').find('input'));
          }
        }" unless opts[:fixed_count]}
        function pasteHandlerFor#{element_id(attr).camelize}(event) {
          event.preventDefault();
          var text = undefined;
          if (window.clipboardData && window.clipboardData.getData) {
            text = window.clipboardData.getData('Text');
          } else {
            text = event.originalEvent.clipboardData.getData('text/plain');
          }
          target = $(event.currentTarget);
          ref_id = target.closest('td').data('num');
          $.each(text.match(/[^\\r\\n]+/g), function(i, row) {
            $.each(row.split('\\t'), function(j, cell) {
              target.val(cell);
              target.closest('td').find('span.val').text(cell);
              new_target = target.closest('td').next('td').find('input');
              if (new_target.length == 0) {
                new_target = target.closest('tr:visible').next('tr:visible').find('td[data-num=\"' + ref_id + '\"] input');
                if (new_target.length == 0) {
                  #{"""insert#{element_id(attr).camelize}Row({after: target.closest('tr:visible')});
                  target = target.closest('tr:visible').next('tr:visible').find('td[data-num=\"' + ref_id + '\"] input');" unless opts[:fixed_count]}
                } else {
                  target = new_target;
                }
                return false;
              } else {
                target = new_target;
              }
            });
            new_target = target.closest('tr:visible').next('tr:visible').find('td[data-num=\"' + ref_id + '\"] input');
            if (new_target.length == 0) {
              #{"""insert#{element_id(attr).camelize}Row({after: target.closest('tr:visible')});
              target = target.closest('tr:visible').next('tr:visible').find('td[data-num=\"' + ref_id + '\"] input');" unless opts[:fixed_count]}
            } else {
              target = new_target;
            }
          });
        }
        function inputHandlerFor#{element_id(attr).camelize}(event) {
          #{"""if (has#{element_id(attr).camelize}Data($(this).closest('tr')) && $(this).closest('tr').next('tr').length == 0) {
            insert#{element_id(attr).camelize}Row({after: $(this).closest('tr')});
          }" unless opts[:fixed_count]}
        }
      </script>
      <table data-max-uid=\"#{trs.count}\" id=\"#{element_id attr}\" class=\"table table-striped table-bordered table-condensed\"><thead><tr>#{ths}</tr></thead><tbody>#{trs.join}#{empty_row}</tbody></table>
      <script>
        set#{element_id(attr).camelize}HandlersFor($(\"##{element_id attr} input\"));
      </script>"
      tpl.html_safe
    end

    def card(level, header, bullets)
      @template.content_tag :div,
        @template.content_tag(:div, header, class: "card-header text-white") +
        @template.content_tag(:ul, bullets.map { |b| @template.content_tag :li, b, class: "list-group-item" }.join.html_safe, class: "list-group list-group-flush"),
        class: "card bg-#{level}"
    end

    def errors
      card :danger, @template.t(:errors_in_data, scope: 'errors.messages'), @object.errors.to_a if @object.errors.any?
    end

    private
    def data_holder_id(attr)
      "#{sanitized_object_name}-#{attr}-data-holder"
    end

    def js_sanitized_object_name(attr)
      "#{sanitized_object_name.camelize}#{attr.to_s.camelize}"
    end

    def parts_for_field_for_multiple(attr, **opts, &block)
      order = opts.delete :order
      rclass = associated_class attr
      current_objects = *@object.public_send(attr).to_a
      current_objects.sort_by! &order if order
      newrecord_form, *forms = [rclass.new, *current_objects].map do |v|
        fields_for attr, v, **opts, &block
      end
      
      newrecord_form.gsub! Regexp.new("(name=\"#{Regexp.escape @object_name}\\[#{attr}_attributes\\])\\[0\\]"), "\\1[#{REPLACEABLE}]"
      newrecord_form.gsub! Regexp.new("(id=\"#{sanitized_object_name}_#{attr}_attributes)_0_"), "\\1_#{REPLACEABLE}_"
      
      [newrecord_form, *forms]
    end
 
    public
    def fields_for_multiple(attr, **opts, &block)
      data_holder = opts.fetch :data_holder, true
      legend = opts.fetch :legend, attr
      forms = parts_for_field_for_multiple attr, **opts, &block
      newrecord_form, *forms = forms.map.with_index do |subform, idx|
        @template.content_tag :div, subform.html_safe + group { secondary_button(:remove, onclick: "remove#{js_sanitized_object_name attr}FormDOM(this, #{idx.zero? ? 'false' : "true, #{idx}"}); return false;") }, class: "#{sanitized_object_name}-#{attr}-form _nested-forms-wrapper"
      end
      
      js = """
      function new#{js_sanitized_object_name attr}Form() {
        holder = $(\"##{data_holder_id attr}\");
        next_id = parseInt(holder.data('max-uid'), 10) + 1;
        holder.data('max-uid', next_id);
        return '#{@template.escape_javascript newrecord_form}'.replace(/#{REPLACEABLE}/g, next_id);
      };
      function remove#{js_sanitized_object_name attr}FormDOM(that, set_destroy_flag, dom_order_idx) {
        var $button_container = $(that).closest('div.form-group');
        var $subform_container = $button_container.closest('div.#{sanitized_object_name}-#{attr}-form');
        if (set_destroy_flag) {
          destroy_input_name = '#{@object_name}[#{attr.to_s}_attributes][' + dom_order_idx + '][_destroy]';
          $subform_container.append('<input type=\"hidden\" name=\"' + destroy_input_name + '\" value=\"1\"/>');
          $subform_container.hide();
          $button_container.hide(); 
        } else {
          $subform_container.remove();
          $button_container.remove();
        }
      }"

      @template.content_tag(:fieldset,
        ((@template.content_tag :legend, legend || attr if legend) || '') +
        @template.content_tag(:script, js.html_safe) +
        ((@template.content_tag :div, nil, id: data_holder_id(attr), data: {'max-uid' => @object.public_send(attr).count} if data_holder) || '') +
        forms.join.html_safe +
        secondary_button("add_#{attr.to_s.singularize}".to_sym, onclick: "$(this).closest(\"div.form-group\").before(new#{sanitized_object_name.camelize}#{attr.to_s.camelize}Form()); return false;")
      )
    end
  end

  class TabPaneAccumulator
    attr_reader :id, :tabs
    def initialize(id)
      @id = id
      @tabs = {}
    end

    def tab(id, text = nil, **opts, &block)
      @tabs[id] = {name: text || id, block: block, opts: opts}
    end
  end

  def tabhead(id, key, text = '', **opts)
    "<li class=\"nav-item\"><a href=\"##{id}-#{key}\" class=\"nav-link#{" active" if opts[:active]}\" data-target=\"##{id}-#{key}\" data-toggle=\"tab\" aria-controls=\"#{id}-#{key}\" role=\"tab\" aria-expanded=\"true\">#{text || k}</a></li>"
  end

  def tabpane(id, key, text, **opts)
    "<div class=\"tab-pane #{' in active show' if opts[:active]}\" id=\"#{id}-#{key}\">#{text}</div>"
  end

  def tabset(id, &block)
    acc = TabPaneAccumulator.new id
    yield acc
    res = ActionView::OutputBuffer.new
    res.safe_concat '<ul class="nav nav-tabs nav-tabs-bordered">'
    acc.tabs.each_with_index { |(k, v), i| res.safe_concat(tabhead id, k, v[:name], **{active: i == 0}.merge(v[:opts])) }
    res.safe_concat '</ul><div class="tab-content tabs-bordered">'
    acc.tabs.each_with_index { |(k, v), i| res.safe_concat(tabpane id, k, with_output_buffer { v[:block].call if v[:block] }, **{active: i == 0}.merge(v[:opts])) }
    res.safe_concat '</div>'
  end

  FLASH_CLASSES = HashWithIndifferentAccess.new(
    alert: 'danger',
    notice: 'info'
  )

  def flash_class(k)
    FLASH_CLASSES[k] || k.to_s
  end

end
